import axios from 'axios'

export default {
    loadWidgetByPosition: function (position) {
        return axios.get('ru/api/public/cms/widget/' + position).then((response) => {
            if (response.status !== 200) {
                return Promise.reject(response);
            }
            return Promise.resolve(response);
        })
    },

    loadBlogBySlug: function (slug, limit = 0) {
        return axios.get('ru/api/public/cms/blog/' + slug + '/' + limit).then((response) => {
            if (response.status !== 200) {
                return Promise.reject(response);
            }
            return Promise.resolve(response);
        })
    },

    loadPageBySlug: function (slug) {
        return axios.get('ru/api/public/cms/article/' + slug).then((response) => {
            if (response.status !== 200) {
                return Promise.reject(response);
            }
            return Promise.resolve(response);
        })
    },
}
