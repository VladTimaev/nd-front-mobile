import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    meta: { layout: "empty" },
    component: () => import('../views/Home.vue')
  },
  {
    path: '/indicators/:slug',
    name: 'indicators',
    meta: { layout: "empty" },
    component: () => import('../views/Indicators.vue')
  },
  {
    path: "/article/:slug",
    name: "article",
    meta: { layout: "empty" },
    component: () => import("../views/Article.vue")
  },
  {
    path: "/blog/:slug",
    name: "blog",
    meta: { layout: "empty" },
    component: () => import("../views/Blog.vue")
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
