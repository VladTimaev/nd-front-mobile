import Vue from 'vue'
import App from './App.vue'
import vuetify from "./plugins/vuetify";

import router from './router'
import store from './store'
import axios from "axios";
import VueFilterDateFormat from 'vue-filter-date-format';
require('dotenv').config();

Vue.use(VueFilterDateFormat, {
  dayOfWeekNames: [
    'Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг',
    'Пятница', 'Суббота'
  ],
  dayOfWeekNamesNamesShort: [
    'Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'
  ],
  monthNames: [
    'Январь(-я)', 'Февраль(-я)', 'Март(-а)', 'Апрель(-я)', 'Май(-я)', 'Июнь(-я)',
    'Июль(-я)', 'Август(-а)', 'Сентябрь(-я)', 'Октябрь(-я)', 'Ноябрь(-я)', 'Декабрь(-я)'
  ],
  monthNamesShort: [
    'Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
    'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'
  ]
});

Vue.prototype.$http = axios;
const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common[
      "Authorization"
      ] = `Bearer ${token}`;
}

axios.defaults.baseURL = process.env.VUE_APP_BACK_URL;
axios.defaults.headers.post["Content-Type"] = "application/json";

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
